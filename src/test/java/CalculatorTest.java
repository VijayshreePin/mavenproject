import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeSuite
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void testSum() {
        Assert.assertEquals(9, calculator.sum(4, 5));
    }

    @Test
    public void testMultiplicaltion() {
        Assert.assertEquals(18, calculator.multiplication(9, 3));
    }
}
